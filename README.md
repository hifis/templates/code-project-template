---
tags  : #consulting
title : A code sample repo
---

# Check Lists

__Minimum Setup__

- [ ] Create a [__`README.md`__](#readme) file
    - [ ] Solid title with a short description (+ badges)
    - [ ] Installation and Requirements section
    - [ ] Usage section
    - [ ] Support section
    - [ ] Contributing section
    - [ ] Authors/Maintainers and acknowledgment section
    - [ ] License section
    - [ ] Project status section

- [ ] Create [__`LICENSE.md`__ and a folder __`LICENSES`__](#licensing) where you should copy the used licenses
- [ ] Create a Contribution guide [__`CONTRIBUTING.md`__](#contributing)
- [ ] `.gitignore`, this files keeps your commits clean of not needed files

In the chapter [README.md template](#readmetemp)

----

__Advanced options for a python repo:__

- [ ] `setup.py` .. needed for publication via or local `pip install`
- [ ] `requirements.txt` for 'installing' the needed packages via `pip`
- [ ] Module folder `<module folder>` (eq. from the [thefuck project][thefuck] `./thefuck`)

__Advanced options in general:__

- [ ] `docs` folder, for further details on the package
- [ ] `test` folder, including testing
- [ ] `.gitlab-ci.yml` to automate linting, testing and building when committing

Further reading: 

- [python-guide.org] 
- Remarks about the [MANIFEST.in] (depicted) ([second comment on stackoverflow][manistack]


# Documentation on the different Files, Folders and Project strategies

## `README.md` {#readme}

The following section is motivated and partially copied from the default suggestion of the [gitlab.com](gitlab.com) team.

Further reading:

- [makeareadme](https://www.makeareadme.com/)
- [standard-readme](https://github.com/RichardLitt/standard-readme)

### Name

Choose a self-explaining name for your project.

### Description

Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

__Badges__

On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

__Visuals__

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

### Requirements and Installation process

Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

### Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

### Support

Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

### Contributing {#contributing}

Due to the extend of the contribution process this section can be stored in a separate file `CONTRIBUTION.md`. See section about [Contribution](#contribution).

In the `README.md` this section one sentence can be found: 

> See `CONTRIBUTION.md` for details on the contribution process.

### Authors and acknowledgment

Show your appreciation to those who have contributed to the project.

### License

For open source projects, say how it is licensed.

### Project status

If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.


## `LICENSE.md` and LICENSE folder {#licensing}

### Why licensing is necessary?

Without the proper licenses, nobody can use or reuse the presented material, created by the author.

### What should you deploy?

- [ ] the general licensing strategy in `LICENSE.md`
- [ ] corresponding licensing text in the folder `LICENSING`
- [ ] SPDX header in all files

### `LICENSE.md`

The `LICENSE.md` describes the license strategies in this project. Corresponding license text is stored in the LICENSE folder.

_Example of the `LICENSE.md`:_

~~~
## License

Copyright (c) <year> <copyright holder>

This work is licensed under multiple licenses:
- The data set is licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).
- The source code and the accompanying material are licensed under [MIT](LICENSES/MIT.txt).
- The documentation and the resulting plots are licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).
- Insignificant files are licensed under [CC0-1.0](LICENSES/CC0-1.0.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
~~~

## `CONTRIBUTING.md` {#contribution}

State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## `setup.py`

__Informations:__

- <https://github.com/kennethreitz/setup.py>
- [What is setup.py?] on Stack Overflow
- [Official Python Packaging User Guide](https://packaging.python.org)
- [The Hitchhiker's Guide to Packaging]
- [Cookiecutter template for a Python package]

# Getting started with your git repo

The following section is take from the default suggestion of the <gitlab.com> team.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin <url>
git branch -M main
git push -uf origin main
```

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/hifis/templates/python-template/-/settings/integrations)


# `README.md` Template

~~~
# <project name>

# Requirements and Installation process

# Usage

# Support

# Contributing

# Authors/Maintainers and acknowledgment

# License

# Project status

~~~


<!-- LINKS -->

[an example setup.py]: https://github.com/navdeep-G/setup.py/blob/master/setup.py
[What is setup.py?]: https://stackoverflow.com/questions/1471994/what-is-setup-py
[The Hitchhiker's Guide to Packaging]: https://the-hitchhikers-guide-to-packaging.readthedocs.io/en/latest/creation.html
[Cookiecutter template for a Python package]: https://github.com/audreyr/cookiecutter-pypackage
[thefuck]: https://github.com/nvbn/thefuck/
[python-guide.org]: https://docs.python-guide.org/writing/structure/
[MANIFEST.in]: https://www.remarkablyrestrained.com/python-setuptools-manifest-in/ 
[manistack]: https://stackoverflow.com/questions/24727709/do-python-projects-need-a-manifest-in-and-what-should-be-in-it

